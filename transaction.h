#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <QtGui/QDialog>

namespace Ui {
    class Transaction;
}

class Transaction : public QDialog {
    Q_OBJECT
public:
    Transaction(QWidget *parent = 0);
    ~Transaction();

    // load a transaction into the form
    void load(int);

    // id = 0 -> INSERT instead of UPDATE
    // all fields are assumed to be validated,
    // only call after the dialog has been accepted
    void save(int = 0);

private:
    void populateContra();
    Ui::Transaction *m_ui;

private slots:
    void on_Contra_currentIndexChanged(int index);
    void on_buttonBox_accepted();
};

#endif // TRANSACTION_H
