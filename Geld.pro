# -------------------------------------------------
# Project created by QtCreator 2009-09-16T14:16:05
# -------------------------------------------------
QT += sql
TARGET = Geld
TEMPLATE = app
SOURCES += main.cpp \
    geld.cpp \
    transactionmodel.cpp \
    transaction.cpp \
    contraaccount.cpp
HEADERS += geld.h \
    transactionmodel.h \
    transaction.h \
    contraaccount.h
FORMS += geld.ui \
    transaction.ui \
    contraaccount.ui
