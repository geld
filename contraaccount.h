#ifndef CONTRAACCOUNT_H
#define CONTRAACCOUNT_H

#include <QtGui/QDialog>

namespace Ui {
    class ContraAccount;
}

class ContraAccount : public QDialog {
    Q_OBJECT
public:
    ContraAccount(QWidget *parent = 0);
    ~ContraAccount();
    void load(int);
    int save(int = 0);

private:
    Ui::ContraAccount *m_ui;
    int itemId;

private slots:
    void on_buttonBox_accepted();
};

#endif // CONTRAACCOUNT_H
