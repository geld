#include "contraaccount.h"
#include "ui_contraaccount.h"
#include <QMessageBox>
#include <QtSql>

ContraAccount::ContraAccount(QWidget *parent) :
    QDialog(parent),
    m_ui(new Ui::ContraAccount)
{
    m_ui->setupUi(this);
    m_ui->Name->setFocus();
    itemId = 0;
}

ContraAccount::~ContraAccount()
{
    delete m_ui;
}

void ContraAccount::on_buttonBox_accepted()
{
    // some validation
    if(m_ui->Name->text().isEmpty()) {
        QMessageBox::critical(this, "Form validation error", "Name field is required.");
        return;
    }
    if(m_ui->Account->text().isEmpty()) {
        QMessageBox::critical(this, "Form validation error", "You know, without an account number, a contra account isn't really an account...");
        return;
    }
    QSqlQuery q;
    q.prepare("SELECT 1 FROM contra_accounts WHERE id <> ? AND lower(name) = lower(?)");
    q.addBindValue(itemId);
    q.addBindValue(m_ui->Name->text());
    q.exec();
    if(q.next()) {
        QMessageBox::critical(this, "Form validation error", "A contra account with that name already exists.");
        return;
    }
    q.prepare("SELECT 1 FROM contra_accounts WHERE id <> ? AND lower(account) = lower(?)");
    q.addBindValue(itemId);
    q.addBindValue(m_ui->Account->text());
    q.exec();
    if(q.next()) {
        QMessageBox::critical(this, "Form validation error", "A contra account with that number already exists.");
        return;
    }
    accept();
}

void ContraAccount::load(int id)
{
    QSqlQuery q;
    q.prepare("SELECT name, account, address FROM contra_accounts WHERE id = ?");
    q.addBindValue(id);
    q.exec();
    q.next();
    m_ui->Name->setText(q.value(0).toString());
    m_ui->Account->setText(q.value(1).toString());
    m_ui->Address->setPlainText(q.value(2).toString());
    itemId = id;
}

int ContraAccount::save(int id)
{
    QSqlQuery q;
    q.prepare(id
        ? "UPDATE contra_accounts SET name = ?, account = ?, address = ? WHERE id = ?"
        : "INSERT INTO contra_accounts (name, account, address) VALUES (?, ?, ?)"
    );
    q.addBindValue(m_ui->Name->text());
    q.addBindValue(m_ui->Account->text());
    q.addBindValue(m_ui->Address->toPlainText());
    if(id)
        q.addBindValue(id);
    q.exec();
    if(!id)
        id = q.lastInsertId().toInt();
    return id;
}
