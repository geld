#include "transaction.h"
#include "ui_transaction.h"
#include <QtSql>
#include <QMessageBox>
#include <QDate>
#include "contraaccount.h"

Transaction::Transaction(QWidget *parent) :
    QDialog(parent),
    m_ui(new Ui::Transaction)
{
    m_ui->setupUi(this);

    populateContra();

    QDoubleValidator *dv = new QDoubleValidator(-1000, 1000, 2, m_ui->Amount);
    m_ui->Amount->setValidator(dv);

    m_ui->Date->setCursorPosition(0);
}

Transaction::~Transaction()
{
    delete m_ui;
}

void Transaction::populateContra() {
    QSqlQuery q;
    q.exec("SELECT id, name FROM contra_accounts ORDER BY name");
    m_ui->Contra->clear();
    m_ui->Contra->addItem(" - ", 0);
    m_ui->Contra->setCurrentIndex(0);
    while(q.next()) {
        m_ui->Contra->addItem(q.value(1).toString(), q.value(0).toInt());
    }
    m_ui->Contra->addItem("...add new contra account", -1);
}

void Transaction::on_buttonBox_accepted()
{
    // some extra input validation - because the validation performed above has its limits
    if(m_ui->Contra->currentIndex() == m_ui->Contra->count()-1) {
        QMessageBox::critical(this, "Form validation error", "Incorrect contra account selected...");
        return;
    }
    QStringList d = m_ui->Date->text().split("-");
    if(!QDate::isValid(d[0].toInt(), d[1].toInt(), d[2].toInt())) {
        QMessageBox::critical(this, "Form validation Error", "Invalid date!");
        return;
    }

    accept();
}

void Transaction::load(int id)
{
    QSqlQuery q;
    q.prepare("SELECT date, amount, contra, description FROM transactions WHERE id = ?");
    q.addBindValue(id);
    q.exec();
    q.next();
    m_ui->Date->setText(q.value(0).toString());
    m_ui->Amount->setText(QString("%1").arg(q.value(1).toDouble()/100, 4, 'f', 2));
    m_ui->Contra->setCurrentIndex(q.value(2).isNull() ? 0 : m_ui->Contra->findData(q.value(2)));
    m_ui->Description->setPlainText(q.value(3).toString());
}

void Transaction::save(int id)
{
    QSqlQuery q;
    q.prepare(id
        ? "UPDATE transactions SET date = ?, amount = ?, contra = ?, description = ? WHERE id = ?"
        : "INSERT INTO transactions (date, amount, contra, description) VALUES (?, ?, ?, ?)"
    );
    q.addBindValue(m_ui->Date->text());
    q.addBindValue(m_ui->Amount->text().toDouble()*100);
    int contra = m_ui->Contra->itemData(m_ui->Contra->currentIndex()).toInt();
    q.addBindValue(contra ? contra : QVariant(QVariant::Int));
    q.addBindValue(m_ui->Description->toPlainText());
    if(id)
        q.addBindValue(id);
    q.exec();
}

void Transaction::on_Contra_currentIndexChanged(int index)
{
    if(m_ui->Contra->itemData(index).toInt() != -1)
        return;

    ContraAccount c(this);
    if(c.exec() == QDialog::Accepted) {
        int id = c.save();
        populateContra();
        m_ui->Contra->setCurrentIndex(m_ui->Contra->findData(id));
    } else {
        m_ui->Contra->setCurrentIndex(0);
    }
}
