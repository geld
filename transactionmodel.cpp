#include "transactionmodel.h"
#include <QDebug>
#include <QStringList>
#include <QtSql>

TransactionModel::TransactionModel(QObject *parent)
        : QSqlQueryModel(parent)
{
    Q_UNUSED(parent);
    refresh();
}

void TransactionModel::refresh() {
    setQuery(
         "SELECT t.id, t.date, t.amount, c.name, t.description "
           "FROM transactions t "
      "LEFT JOIN contra_accounts c ON c.id = t.contra "
       "ORDER BY t.date DESC"
    );
    setHeaderData(0, Qt::Horizontal, "ID");
    setHeaderData(1, Qt::Horizontal, "Date");
    setHeaderData(2, Qt::Horizontal, "Amount");
    setHeaderData(3, Qt::Horizontal, "Contra account");
    setHeaderData(4, Qt::Horizontal, "Description");
}

QVariant TransactionModel::data(const QModelIndex &index, int role) const
{
    QVariant val = QSqlQueryModel::data(index, role);
    if(role == Qt::DisplayRole && index.column() == 2) {
        return QString("%1").arg(val.toDouble()/100, 4, 'f', 2);
    }
    if(role == Qt::ForegroundRole && index.column() == 2) {
        double dat = QSqlQueryModel::data(index, Qt::DisplayRole).toDouble();
        return dat > 0 ? Qt::green : Qt::red;
    }
    if(role == Qt::TextAlignmentRole) {
        switch(index.column()) {
            case 0: return (int)Qt::AlignVCenter|Qt::AlignRight;
            case 1: return (int)Qt::AlignVCenter|Qt::AlignLeft;
            case 2: return (int)Qt::AlignVCenter|Qt::AlignRight;
            case 3: return (int)Qt::AlignVCenter|Qt::AlignLeft;
            case 4: return (int)Qt::AlignVCenter|Qt::AlignLeft;
        }
    }
    return val;
}
