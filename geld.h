#ifndef GELD_H
#define GELD_H

#include <QtGui/QMainWindow>
#include <QtSql>

namespace Ui
{
    class Geld;
}

class Geld : public QMainWindow
{
    Q_OBJECT

public:
    Geld(QWidget *parent = 0);
    ~Geld();

private:
    void initDB(QString f);
    void closeDB();
    int selectedTransaction();
    int selectedContra();
    void refreshLists();
    Ui::Geld *ui;

private slots:
    void addTransaction();
    void editTransaction();
    void deleteTransaction();
    void addContra();
    void editContra();
    void deleteContra();
};

#endif // GELD_H
