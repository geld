#ifndef TRANSACTIONMODEL_H
#define TRANSACTIONMODEL_H

#include <QSqlQueryModel>

class TransactionModel : public QSqlQueryModel
{
public:
    TransactionModel(QObject * parent = 0);
    void refresh();
    QVariant data(const QModelIndex &index, int role) const;
};

#endif // TRANSACTIONMODEL_H
